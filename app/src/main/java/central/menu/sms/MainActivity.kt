package central.menu.sms

import android.Manifest
import android.app.AlertDialog
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.*
import android.support.v7.app.AppCompatActivity
import android.content.pm.PackageManager
import android.os.*
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.text.TextUtils
import android.widget.EditText
import java.io.IOException
import androidx.work.impl.Schedulers.schedule






class MainActivity : AppCompatActivity() {
    private val REQUEST_CODE = 101
    private val allPermisions : ArrayList<String> = ArrayList()
    private var mServiceComponent: ComponentName? = null
    private var mHandler : Handler = Handler();

    val WORK_DURATION_KEY = BuildConfig.APPLICATION_ID + ".WORK_DURATION_KEY"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mServiceComponent = ComponentName(this, SendService::class.java)
        setContentView(R.layout.activity_main)

        this.initService();
    }

    private fun initService(){
        setupPermissionsAndLaunchService(
            arrayOf(
                Manifest.permission.SEND_SMS,
                Manifest.permission.INTERNET,
                Manifest.permission.MODIFY_PHONE_STATE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE));
    }

    override fun onResume(){
        super.onResume();


    }

    private fun setupLog(){
        try {
            MyLogger.setup(this)
        } catch (e: IOException) {
            e.printStackTrace()
            throw RuntimeException("Problems with creating the log files")
        }
    }

    private fun setupPermissionsAndLaunchService(permissions : Array<String>) {

        if ( ContextCompat.checkSelfPermission(
                this,
                permissions[0]) != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    permissions[0])) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                showMessageOKCancel(permissions, "You need to allow access to " );
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    permissions,
                    REQUEST_CODE)
            }
            Log.i("permissions", "Permission to record denied")
        } else {
            launchTestService();
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    launchTestService()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }


    /**
     * Executed when user clicks on SCHEDULE JOB.
     */
    fun launchTestService() {
        setupLog();

        setupLog();

        scheduleJob()

    }

    fun scheduleJob(){
        /* Create an Intent that will start the Menu-Activity. */
        var JOB_ID : Int = 1001
        //var TIME_INTERVAL : Long = JobInfo.getMinPeriodMillis();
        var TIME_INTERVAL : Long =JobInfo.getMinPeriodMillis();
        val builder = JobInfo.Builder(jobId++, mServiceComponent)
        builder.setMinimumLatency(2000)
        builder.setOverrideDeadline(10000)
        builder.setRequiresDeviceIdle(false)
        builder.setRequiresCharging(false)
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
        val tm = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        tm.schedule(builder.build())
    }

    fun bkplaunchTestService() {
        setupLog();
        mHandler!!.postDelayed({
            /* Create an Intent that will start the Menu-Activity. */
            var JOB_ID : Int = 1001
            //var TIME_INTERVAL : Long = JobInfo.getMinPeriodMillis();
            var TIME_INTERVAL : Long =JobInfo.getMinPeriodMillis();
            val jobInfo: JobInfo
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                jobInfo = JobInfo.Builder(JOB_ID, mServiceComponent)
                    .setPeriodic(TIME_INTERVAL)
                    .build()
            } else {
                jobInfo = JobInfo.Builder(JOB_ID, mServiceComponent)
                    .setPeriodic(TIME_INTERVAL)
                    .build()
            }
            val tm = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
            tm.schedule(jobInfo)




        }, 3000)


    }

    private fun showMessageOKCancel(permissions : Array<String>, message: String) {

        allPermisions.addAll( permissions)
        AlertDialog.Builder(this@MainActivity)
            .setMessage(message)
            .setPositiveButton("OK", listener)
            .setNegativeButton("Cancel", listener)
            .create()
            .show()
    }

    fun requestPermissions(){
        ActivityCompat.requestPermissions(
            this,
            allPermisions.toArray() as Array<out String>,
            REQUEST_CODE)
    }

    var listener: DialogInterface.OnClickListener = object : DialogInterface.OnClickListener {

        internal val BUTTON_NEGATIVE = -2
        internal val BUTTON_POSITIVE = -1

        override fun onClick(dialog: DialogInterface, which: Int) {
            when (which) {
                BUTTON_NEGATIVE ->
                    // int which = -2
                    dialog.dismiss()

                BUTTON_POSITIVE -> {

                    // int which = -1
                    requestPermissions()
                    dialog.dismiss()
                }
            }
        }
    }
    companion object {
        var jobId = 0;
    }


    override fun onStop() {
        // A service can be "started" and/or "bound". In this case, it's "started" by this Activity
        // and "bound" to the JobScheduler (also called "Scheduled" by the JobScheduler). This call
        // to stopService() won't prevent scheduled jobs to be processed. However, failing
        // to call stopService() would keep it alive indefinitely.
        stopService(Intent(this, SendService::class.java))
        super.onStop()
    }


    override fun onStart() {
        super.onStart()
        // Start service and provide it a way to communicate with this class.
        val startServiceIntent = Intent(this, SendService::class.java)
        startService(startServiceIntent)
    }
}

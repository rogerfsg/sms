package central.menu.sms

import android.Manifest
import android.app.*
import android.app.job.JobInfo
import android.app.job.JobParameters
import android.app.job.JobScheduler
import android.app.job.JobService
import android.content.ComponentName
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.Context
import android.support.v4.app.NotificationCompat
import android.support.annotation.RequiresApi
import java.util.*
import android.content.pm.PackageManager
import android.os.*
import android.support.v4.content.ContextCompat
import android.telephony.SmsManager
import android.text.TextUtils
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.util.logging.Logger


/**
 * An [IntentService] subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
//class SendService : IntentService("SendService") {
class SendService : JobService() {

    private val LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)

//    private var mServiceComponent: ComponentName? = null
//    val MESSENGER_INTENT_KEY = BuildConfig.APPLICATION_ID + ".MESSENGER_INTENT_KEY"
//    val WORK_DURATION_KEY = BuildConfig.APPLICATION_ID + ".WORK_DURATION_KEY"

    override fun onStartJob(p0: JobParameters?): Boolean {
        LOGGER.info("on start job" )
//        initForegroundService();
//        if(iteracao++ > 0){
//            for(i in 0..340){
                CheckNewSmssToSend();
//                Thread.sleep(2500);
//            }
//        }
//        jobFinished(p0, true);
        scheduleJobV2();
        return true;
    }

    fun scheduleJobV2() {

        val builder = JobInfo.Builder(mJobId++, ComponentName(this, SendService::class.java))

        builder.setMinimumLatency(45000)
        builder.setOverrideDeadline(60000)

        val requiresUnmetered = false
        val requiresAnyConnectivity = true
        if (requiresUnmetered) {
            builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
        } else if (requiresAnyConnectivity) {
            builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
        }
        builder.setRequiresDeviceIdle(false)
        builder.setRequiresCharging(false)

        // Extras, work duration.
        val extras = PersistableBundle()
        var workDuration = "2000"
        if (TextUtils.isEmpty(workDuration)) {
            workDuration = "1"
        }


        builder.setExtras(extras)

        // Schedule job
        Log.d(TAG, "Scheduling job")
        val tm = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        tm.schedule(builder.build())
    }


    override fun onStopJob(p0: JobParameters?): Boolean {
        Log.i(TAG, "on stop job" )



//         To reschedule
        return true
    }


    private fun canSendSms(): Boolean {
        LOGGER.info("canSendSms");
        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.SEND_SMS)

        return permission == PackageManager.PERMISSION_GRANTED;
    }

    fun sendSMS(phoneNumber: String, message: String) {
        LOGGER.info("sendSMS: " + i )
        if(!canSendSms()) {

            LOGGER.severe("Not allowed to send sms: " +i++)
            return;
        }
        val SENT = "SMS_SENT"
        val DELIVERED = "SMS_DELIVERED"

        val sentPI = PendingIntent.getBroadcast(
            this, 0,
            Intent(SENT), 0
        )

        val deliveredPI = PendingIntent.getBroadcast(
            this, 0,
            Intent(DELIVERED), 0
        )

        val sms = SmsManager.getDefault()
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI)
    }

//    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//        LOGGER.info("onStartCommand: " + i )
//        initForegroundService();
//
//        return Service.START_REDELIVER_INTENT;
//    }


    override fun onDestroy() {
        LOGGER.info("onDestroy: " + i )
        super.onDestroy()

//        stopForeground(true)

    }

//    fun initForegroundService(){
//        LOGGER.info("initForegroundService: " + i )
//        //For creating the Foreground Service
//        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//        val channelId =
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) getNotificationChannel(notificationManager) else ""
//        val notificationBuilder = NotificationCompat.Builder(this, channelId)
//        val notification = notificationBuilder.setOngoing(true)
//            .setSmallIcon(R.mipmap.ic_launcher)
//            // .setPriority(PRIORITY_MIN)
//            .setCategory(NotificationCompat.CATEGORY_SERVICE)
//            .build()
//
//        startForeground(110, notification);
//    }

//    @RequiresApi(Build.VERSION_CODES.O)
//    private fun getNotificationChannel(notificationManager: NotificationManager): String {
//        LOGGER.info("getNotificationChannel: " + i )
//        val channelId = "channelid"
//        val channelName = resources.getString(R.string.app_name)
//        val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
//        channel.importance = NotificationManager.IMPORTANCE_NONE
//        channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
//        notificationManager.createNotificationChannel(channel)
//        return channelId
//    }

    fun CheckNewSmssToSend()
    {
        try {
            // Instantiate the RequestQueue.
            val queue = Volley.newRequestQueue(this)

            val url = getString(R.string.api) + "sms.php"

            // Request a string response from the provided URL.
            val stringRequest  = StringRequest(
                Request.Method.POST, url,
                Response.Listener<String> { response ->
                    try{

                        var smss= JSONArray(response)

                        for(i in 0..smss.length() - 1 ){
                            var json : String = smss.getString(i) ;
                            var obj = JSONObject(json)
                            sendSMS(obj.getString("idRecipient"), obj.getString("msg"))
                        }

                        if(i++ % 50 == 0)
                            LOGGER.info("OK: " + i )
                    }catch (ex: Exception){
                        LOGGER.severe("SendService " +i + ": " + ex.message + "::" + ex.toString())
                    }
                },
                Response.ErrorListener {
                    println( "That didn't work!"+ i  )
                    LOGGER.warning("That didn't work!" + i )
                })

            // Add the request to the RequestQueue.
            queue.add(stringRequest)

        }catch (ex: Exception){
            //Log.w("SendService", ex.message);
            LOGGER.severe("SendService "+i.toString()+": " + ex.message  + "::" + ex.toString())
        }
    }

    companion object {

        var iteracao = 0;
        var i = 0;
        var mJobId =1;
    }

}



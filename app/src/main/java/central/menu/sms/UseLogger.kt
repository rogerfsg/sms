package central.menu.sms

import java.io.IOException
import java.util.logging.Level
import java.util.logging.Logger


class UseLogger {


//    fun doSomeThingAndLog() {
//        // ... more code
//
//        // now we demo the logging
//
//        // set the LogLevel to Severe, only severe Messages will be written
//        LOGGER.setLevel(Level.SEVERE)
//        LOGGER.severe("Info Log")
//        LOGGER.warning("Info Log")
//        LOGGER.info("Info Log")
//        LOGGER.finest("Really not important")
//
//        // set the LogLevel to Info, severe, warning and info will be written
//        // finest is still not written
//        LOGGER.setLevel(Level.INFO)
//        LOGGER.severe("Info Log")
//        LOGGER.warning("Info Log")
//        LOGGER.info("Info Log")
//        LOGGER.finest("Really not important")
//    }
//
//    companion object {
////        https://stackoverflow.com/questions/40352684/what-is-the-equivalent-of-java-static-methods-in-kotlin
////         use the classname for the logger, this way you can refactor
//        private val LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
//
//        @JvmStatic
//        fun main(args: Array<String>) {
//            val tester = UseLogger()
//            try {
//                MyLogger.setup()
//            } catch (e: IOException) {
//                e.printStackTrace()
//                throw RuntimeException("Problems with creating the log files")
//            }
//
//            tester.doSomeThingAndLog()
//        }
//    }
}

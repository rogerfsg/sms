package central.menu.sms

import android.content.Context
import android.os.Environment
import java.io.File
import java.util.logging.*

class MyLogger {

    companion object {
        private lateinit var fileTxt : FileHandler;
        private val formatterTxt : SimpleFormatter = SimpleFormatter();

        fun setup(context : Context) {

            val logger : Logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
            logger.setLevel(Level.ALL);
            var f : File = context.getExternalFilesDir(Environment.DIRECTORY_DCIM)

            val sdf = java.text.SimpleDateFormat("yyyyMMddHHmmss")
            val date = java.util.Date()


            var pathFile : String = f.absolutePath+ "/Logging" +sdf.format(date) + ".txt"
            //println(pathFile)
            fileTxt = FileHandler(pathFile);
            fileTxt.setFormatter(formatterTxt);
            logger.addHandler(fileTxt);

        }
    }
}

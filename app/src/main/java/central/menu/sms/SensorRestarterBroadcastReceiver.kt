package central.menu.sms

import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.ContextWrapper
import java.util.logging.Logger


class SensorRestarterBroadcastReceiver : BroadcastReceiver() {
    private val LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
    override fun onReceive(context: Context, intent: Intent) {
        LOGGER.info("Service Stops! Oops!!!!")
        var c = ContextWrapper(context)
        c.startService(Intent(context, SendService::class.java))

    }
}